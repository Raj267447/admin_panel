import React from "react";
import { Grid ,Button ,Typography} from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    height:"80vh",
    width:'100%',
    display:'flex',
  },
  form:{
    width:'100%'
  },
  container: {
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    color: "white",
  },
  paragraph: {
    color: "white",
  },
  input:{
    outline:'none',
    width:'300px',
    border:'1px solid grey',
    borderRadius:'30px',
    backgroundColor:'white',
    padding:"10px",
    '&: hover':{
      backgroundColor:'grey'
    }

  },
  btn:{
    color:"black",
    backgroundColor:"white" ,
    border:'1px solid grey',
    borderRadius:"20px",
    '& :: hover':{
      backgroundColor:'grey',
    }
  }
}));

function Form() {
  const classes = useStyles();

  const handleOnChange = () => {};

  return (
    <div className={classes.root}>
      <form action="#" method="GET" className={classes.form}>
        <Grid container spacing={2} className={classes.container} xs={12}>
          <Grid item >
            <Typography variant="h4" className={classes.header}>
              Sign In
            </Typography>
          </Grid>
          <Grid item>
            <Typography className={classes.paragraph} variant="body2">
              Sign In with your mobile no. and OTP
            </Typography>
          </Grid>
          <Grid item>
            <input
              className={classes.input}
              id="standard-disabled"
              placeholder="Mobile no. (10 digits)"
              onChange={handleOnChange}
              
            />
          </Grid>
          <Grid>
            <Button variant="contained" className={classes.btn} >Send OTP</Button>
          </Grid>
        </Grid>
      </form>
    </div>
  );
}

export default Form;
