import React from 'react'
import '../css/signIn-css/SignIn.css'
import Image from './common-components/image-component/Image';
import Form  from "./signIn-components/form/Form"


function SignIn() {
    return (
        <div className="sign-in">
            <Image title="INCRED" img="./icons/IncredwhiteNone.png" height='70px' width='200px' />
            <Form />
        </div>
    )
}

export default SignIn 
