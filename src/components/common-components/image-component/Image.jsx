import { makeStyles, CardMedia } from '@material-ui/core'
import React from 'react'

const useStyle = makeStyles(() =>({
    root:{
      zIndex:'99',
      
    },
    image:{
      backgroundSize:'contain',
    },
    
}))

function Image(props) {
    const classes = useStyle();

    return (
        <div className={classes.root}>
            <CardMedia 
              className={classes.image}
              title={props.title}
              image={props.img}
              style={{height: props.height ? props.height :'auto' ,width: props.width ? props.width:'auto'}}
            />           
        </div>
    )
}

export default Image 
