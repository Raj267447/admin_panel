// test file to  check how new featers perform with project are working with on project

import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';

const useStyle = makeStyles((themes) => ({
    root : {
        display:'flex',
        width:'100%',
        height:'100%',
        alignItems:'center'
    },
    text: {
        textAlign:'center'

    }
}))
function Test() {
    const classes = useStyle();
    
    return (
        <div className={classes.root}>
            <Typography className={classes.text} variant="h2" color="initial">Test File</Typography>        
        </div>
    )
}

export default Test
