const SideBar = [
  {
    id: "#1",
    listItem: "ADMIN",
    ico: "./icons/admin.png",
    to: "/panel",
  },
  {
    id: "#2",
    listItem: "Campaign",
    ico: "./icons/Campaign.png",
    to: "/panel",
  },
  {
    id: "#3",
    listItem: "Packages",
    ico: "./icons/Packages.png",
    to: "/panel",
  },
  {
    id: "#4",
    listItem: "Company Management",
    ico: "./icons/Company.png",
    to: "/panel",
  },
  {
    id: "#5",
    listItem: "Advertiser Management",
    ico: "./icons/AdvertiserManagement.png",
    to: "/panel",
  },
  {
    id: "#6",
    listItem: "Influencer Management",
    ico: "./icons/influencerManagement.png",
    to: "/panel",
  },
];

export { SideBar };
