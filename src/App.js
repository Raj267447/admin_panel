// test component
import Test from "./components/Test";
// project component
import React from "react";
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import SignIn from "./components/SignIn";
import Panel from "./components/Panel";

function App() {
  return (
    <Router>
      <div className="app">
        <Switch>
          {/* -- test components route -- */}
          <Route path="/test">
            <Test />
          </Route>

          {/* -- project components route -- */}

          <Route path="/panel">
            <Panel />
          </Route>
          <Route path="/">
            <SignIn />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
